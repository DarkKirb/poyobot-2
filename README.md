# poyobot

Description goes here

## Getting started

Link to documentation, etc

## Built with

- X software
- Y software

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning.

## Authors

- Charlotte D <darkkirb@gmail.com> - *initial work*

## License

This project is licensed under the 2-Clause BSD License - see the [LICENSE](LICENSE) file for details.

## Acknowledgements

Insert text here.

//! Websocket connection

/// Kind of websocket connection
pub enum Kind {
    Etf,
    Json,
}
